package business.repository;

import business.model.Perfil;
import org.springframework.data.repository.CrudRepository;

public interface PerfilRepository extends CrudRepository<Perfil,Long> {
}
