package business.web.controller;

import business.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class EscuelaController {

    @Autowired
    private IUsuarioService usuarioService;

    @RequestMapping("")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("escuela");
        modelAndView.addObject("usuarios", usuarioService.getAll());
        return modelAndView;
    }

    @GetMapping("filter")
    public ModelAndView filter(@ModelAttribute("nombre") String nombre, @ModelAttribute("paterno") String apPaterno) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(nombre.equals("") && apPaterno.equals("") ? "redirect:/" : "escuela");
        modelAndView.addObject("usuarios", usuarioService.findUsuarioByNombreOrApPaterno(nombre, apPaterno));
        return modelAndView;
    }

    @GetMapping("delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        usuarioService.deleteUserById(id);
        modelAndView.setViewName("redirect:/");
        return modelAndView;
    }
}
