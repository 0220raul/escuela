package business.service;

import business.model.Usuario;
import business.repository.UsuarioRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService implements IUsuarioService{

    UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public List<Usuario> getAll() {
         return usuarioRepository.findAll();
    }

    @Override
    public List<Usuario> findUsuarioByNombreOrApPaterno(String nombre, String apPaterno) {
        return usuarioRepository.findUsuarioByNombreOrApPaterno(nombre, apPaterno);
    }

    @Override
    public void deleteUserById(Long id) {
        usuarioRepository.deleteById(id);
    }

}
