package business.service;

import business.model.Usuario;

import java.util.List;

public interface IUsuarioService {
    List<Usuario> getAll();
    List<Usuario> findUsuarioByNombreOrApPaterno(String nombre, String apPaterno);
    void deleteUserById(Long id);
}
