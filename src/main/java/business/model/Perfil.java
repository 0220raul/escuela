package business.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "perfil")
public class Perfil implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_perfil")
    private Long idPerfil;
    @Column(name = "nombre", length = 20)
    private String nombre;

    @OneToOne(mappedBy = "perfil", cascade = CascadeType.ALL)
    private Usuario usuario;

    public Long getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Long idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
