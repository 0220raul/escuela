USE
escuela_bd;

INSERT INTO perfil (nombre)
VALUES ('Perfil1'),
       ('Perfil2'),
       ('Perfil3'),
       ('Perfil4'),
       ('Perfil5');

INSERT INTO usuario (ap_materno, ap_paterno, nombre, id_perfil)
VALUES ('Villanueva', 'Sancho', 'Afonso', 5),
       ('Villena', 'Valenzuela', 'Juan', 1),
       ('Montes', 'Mejia', 'Andres', 2),
       ('Torre', 'Quesada', 'Jordi', 3),
       ('Redondo', 'Quintana', 'Alexandra', 4);
GO